import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    application
    id("org.beryx.runtime") version "1.8.0"
}

group = "io.jekyllfy"
version = "0.4.1"

repositories {
    mavenCentral()
    jcenter()
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}


sourceSets {
    create("intTest") {
        compileClasspath += sourceSets.main.get().output
        runtimeClasspath += sourceSets.main.get().output
    }
}

val intTestImplementation by configurations.getting {
    extendsFrom(configurations.implementation.get())
}

val intTestRuntimeOnly = configurations["intTestRuntimeOnly"].extendsFrom(configurations.runtimeOnly.get())

val integrationTest = task<Test>("integrationTest") {
    description = "Runs integration tests."
    group = "verification"

    testClassesDirs = sourceSets["intTest"].output.classesDirs
    classpath = sourceSets["intTest"].runtimeClasspath
    shouldRunAfter("test")
}

tasks.check { dependsOn(integrationTest) }

dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation(platform("com.fasterxml.jackson:jackson-bom:[2.9,2.10["))

    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("io.github.microutils:kotlin-logging:1.7.8")
    implementation("org.apache.httpcomponents:httpclient:[4.5,4.6)")
    implementation("org.apache.commons:commons-compress:1.19")
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("com.github.ajalt:clikt:2.3.0")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.5.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.5.2")
    intTestImplementation("org.junit.jupiter:junit-jupiter-api:5.5.2")
    intTestRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.5.2")
}

application {
    mainClassName = "jekyllfy.AppKt"
}

val copyImage by tasks.register<Copy>("copyImage") {
    dependsOn("runtime")
    from("$buildDir/image")
    into("$buildDir/jekyllfy-${project.version}")
}

val imageTarball by tasks.register<Tar>("imageTarball") {
    dependsOn(copyImage)
    from("$buildDir/jekyllfy-${project.version}")
    into("jekyllfy-${project.version}")
    destinationDirectory.set(buildDir)
    archiveBaseName.set("jekyllfy")
    archiveClassifier.set(System.getProperty("os.arch"))
    compression = Compression.GZIP
}

val imageZip by tasks.register<Zip>("imageZip") {
    dependsOn(copyImage)
    from("$buildDir/jekyllfy-${project.version}")
    into("jekyllfy-${project.version}")
    destinationDirectory.set(buildDir)
    archiveBaseName.set("jekyllfy")
    archiveClassifier.set(System.getProperty("os.arch"))
}

tasks.named<Delete>("clean") {
    delete.add("$buildDir/image")
    delete.add("$buildDir/jekyllfy-${project.version}.tar.gz")
}
