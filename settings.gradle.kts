rootProject.name = "jekyllfy"

pluginManagement {
    val kotlinVersion = "1.3.61"
    plugins {
        kotlin("jvm") version kotlinVersion
        kotlin("plugin.spring") version kotlinVersion
    }
}
