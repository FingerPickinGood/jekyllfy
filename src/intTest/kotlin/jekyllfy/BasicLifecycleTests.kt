package jekyllfy

import jekyllfy.exec.*
import mu.KotlinLogging
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Assertions.fail
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import java.io.File

/**
 * The basic lifecycle doesn't really use extensions.
 *
 * Major stages:
 *
 * 1. `jekyllfy mkimage`
 * 2. `jekyllfy run new .`
 * 3. `jekyllfy run build`
 * 4. `jekyllfy clean`
 *
 * For Linux, these tests assume NOPASSWD sudo access.
 *
 * We generally just use the native image build.
 */
@TestInstance(PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class BasicLifecycleTests {

    lateinit var execInherit: ExecInherit
    lateinit var execPipe: ExecPipe
    lateinit var tmpDir: File
    lateinit var jekyllfyBin: String

    @BeforeAll
    fun init() {
        execInherit = ExecInheritInternal(isSudo = false)
        execPipe = ExecPipeInternal(isSudo = false)
        tmpDir = createTempDir()
        extractJekyllfy()
    }

    @AfterAll
    fun cleanup() {
        tmpDir.deleteRecursively()
    }

    @Test
    @DisplayName("jekyllfy mkimage")
    @Order(100)
    fun jekyllfyMkimage() {
        execInherit(jekyllfyBin, "mkimage", directory = tmpDir, timeout = 0)
                .orFail("jekyllfy mkimage failed")
    }

    @Test
    @DisplayName("jekyllfy run new myproj")
    @Order(200)
    fun jekyllfyRunNew() {
        execInherit(jekyllfyBin, "run", "new", "myproj", directory = tmpDir, timeout = 0L)
                .orFail("jekyllfy run new failed")

        tmpDir.listFiles().forEach { println("file: ${it.absolutePath}") }
    }

    @Test
    @DisplayName("jekyllfy run build")
    @Order(300)
    fun jekyllfyRunBuild() {
        // NOTE: we actually have to run --update since we're running in a
        // separate directory.
        val projDir = tmpDir.resolve("myproj")
        execInherit(jekyllfyBin, "run", "--update", "build", directory = projDir, timeout = 0L)
                .orFail("jekyllfy run build failed")

        assertTrue(projDir.resolve("_site").exists(), "_site wasn't created")
    }

    @Test
    @DisplayName("jekyllfy clean")
    @Order(400)
    fun jekyllfyClean() {
        execInherit(jekyllfyBin, "clean", directory = tmpDir, timeout = 0L)
                .orFail("jekyllfy clean")
    }
}

private val log = KotlinLogging.logger { }

private fun ExecResult.orFail(msg: String) {
    when (this) {
        is ExecOk -> {}
        else -> fail(msg)
    }
}

private fun BasicLifecycleTests.extractJekyllfy() {
    val latest = File("build")
            .listFiles { f -> f.extension == "tgz" }
            ?.toList()
            ?.maxBy { it.lastModified() }
            ?: error("No builds detected")

    log.info { "Using latest distribution $latest"}

    execInherit("tar", "xf", latest.absolutePath, directory = tmpDir)
            .orFail("tar xf ${latest.absolutePath} failed")

    val nameWithArch = latest.nameWithoutExtension
    val nameWithoutArch = nameWithArch.substring(0, nameWithArch.lastIndexOf("-"))
    jekyllfyBin = tmpDir.resolve(nameWithoutArch).resolve("bin/jekyllfy").absolutePath

    log.info { "jekyllfyBin: $jekyllfyBin"}
}
