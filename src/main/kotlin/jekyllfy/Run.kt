package jekyllfy

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import jekyllfy.exec.*
import mu.KotlinLogging
import java.io.File

/**
 * The `run` command basically passes all of it's arguments to the container
 * for running, just with an option for running within a particular directory
 * context.
 *
 * This will cleanup any container after it's done (--rm).
 */
class Run(
        val execInherit: ExecInherit,
        val execPipe: ExecPipe
) : CliktCommand("Execute `jekyll` command in a container (then delete it)") {

    val update by option(help = "Runs bundle update before proceeding").flag(default = false)

    val jekyllArgs by argument().multiple()
    private val config by requireObject<Map<String, String>>()

    override fun run() = runCommand("jekyllfy run") {
        log.debug { "jekyllArgs: $jekyllArgs" }

        val label = config["LABEL"] ?: error("LABEL is not set")
        log.debug { "image label: $label" }

        val workingDir = config["WORKING_DIR"] ?: error("WORKING_DIR is not set")
        log.debug { "workingDir: $workingDir" }

        val cwdFile = File(workingDir)

        // When we create a container for the project, we save the ID in
        // .container.
        val cidFile = cwdFile.resolve(".container")

        // Determine the current container ID, creating it if it doesn't
        // seem to exist.
        val containerId = if (!cidFile.exists()) {
            createContainerForDir(cwdFile, label)
        } else if (!containerExists(cidFile.readText())) {
            cidFile.delete()
            createContainerForDir(cwdFile, label)
        } else {
            cidFile.readText().trim()
        }
        cidFile.writeText(containerId)

        // Check and ensure the container is running before exec-ing the
        // wanted command.
        if (!isContainerRunning(containerId)) {
            startContainer(containerId)
        }

        if (update)
            execBundleUpdate(containerId, cwdFile)

        execRunCommand(containerId, cwdFile, jekyllArgs)

        // NOTE: We _may_ want to consider an option to leave the container
        // running, but, I suspect there may not be many cases this is
        // actually useful.
        stopContainer(containerId)
    }
}

private val log = KotlinLogging.logger { }

private fun Run.containerExists(containerId: String): Boolean {
    val cmdLine = arrayOf("docker", "ps", "-a", "-q",
            "--filter", "id=$containerId")
    val (result, stdout) = execPipe(*cmdLine)
    return when (result) {
        is ExecOk -> stdout.trim().isNotEmpty()
        is ExecFailed -> error("docker ps failed with result ${result.exitCode}, stdout: $stdout")
        is ExecTimeout -> error("docker ps timed out")
    }
}

/**
 * This launches a container that will basically run bash in the background.
 *
 * This lets us start a container in the background and detach it, without
 * actually doing anything, so that the later `exec` calls will do the real
 * work.
 */
private fun Run.createContainerForDir(cwd: File, label: String): String {
    val cmdLine = arrayOf("docker", "create",
            "-v", "${cwd.canonicalPath}:/srv/jekyllfy",
            "-w", "/srv/jekyllfy",
            label,
            "tail", "-f", "/dev/null")
    val (result, stdout) = execPipe(*cmdLine)
    return when (result) {
        is ExecOk -> stdout.trim()
        is ExecFailed -> error("docker create failed with result ${result.exitCode}, stdout: $stdout")
        is ExecTimeout -> error("docker create timed out")
    }
}

private fun Run.isContainerRunning(containerId: String): Boolean {
    val cmdLine = arrayOf("docker", "ps", "-a",
            "--filter", "id=$containerId",
            "--format", "{{.Status}}")

    val (result, stdout) = execPipe(*cmdLine)

    return when (result) {
        is ExecOk -> stdout.startsWith("Up")
        is ExecFailed -> false
        is ExecTimeout -> error("docker ps -a... timed out checking status (uh oh)")
    }
}

private fun Run.startContainer(containerId: String) {
    val cmdLine = arrayOf("docker", "start",
            containerId)

    val (result, stdout) = execPipe(*cmdLine)

    when (result) {
        is ExecOk -> {
        }
        is ExecFailed -> error("failed to start container $containerId: $stdout")
        is ExecTimeout -> error("docker start $containerId timed out")
    }
}

private fun Run.execBundleUpdate(containerId: String, cwd: File) {
    val cmdLine = arrayOf("docker", "exec", containerId,
            "bundle", "update")

    when (val result = execInherit(*cmdLine, directory = cwd, timeout = 0)) {
        is ExecOk -> {}
        is ExecFailed -> error("bundle update failed with error ${result.exitCode}")
        is ExecTimeout -> error("bundle update timed out")
    }
}

private fun Run.execRunCommand(containerId: String, cwd: File, args: List<String>) {
    val cmdLine = arrayOf("docker", "exec", containerId,
            "jekyll",
            *args.toTypedArray())

    when (val result = execInherit(*cmdLine, directory = cwd, timeout = 0)) {
        is ExecOk -> {
        }
        is ExecFailed -> error("jekyll run failed with exit ${result.exitCode}")
        is ExecTimeout -> error("illegal timeout hit")
    }
}

private fun Run.stopContainer(containerId: String) {
    val cmdLine = arrayOf("docker", "stop", "-t", "0", containerId)

    val (result, stdout) = execPipe(*cmdLine)

    when (result) {
        is ExecOk -> {}
        is ExecFailed -> error("docker stop failed with exit ${result.exitCode}:\n$stdout")
        is ExecTimeout -> error("illegal timeout hit")
    }
}
