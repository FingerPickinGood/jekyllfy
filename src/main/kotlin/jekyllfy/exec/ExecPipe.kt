package jekyllfy.exec

import mu.KotlinLogging
import java.io.File
import java.util.concurrent.TimeUnit

interface ExecPipe {

    operator fun invoke(vararg cmdLine: String,
                        directory: File? = null,
                        timeout: Long = 60L,
                        timeUnit: TimeUnit = TimeUnit.SECONDS): Pair<ExecResult, String>
}

class ExecPipeInternal(val isSudo: Boolean): ExecPipe {

    override fun invoke(vararg cmdLine: String, directory: File?, timeout: Long, timeUnit: TimeUnit): Pair<ExecResult, String> =
        doExecPipe(*cmdLine, isSudo = isSudo, directory = directory, timeout = timeout, timeUnit = timeUnit)
}

/**
 * Wraps the ProcessBuilder, and uses inherited IO to capture the stdout
 *
 * @param cmdline The main command line
 * @param isSudo If true, just prepends "sudo" to the commandline
 * @param timeout If > 0, issues a timed wait before returning an ExecTImeout
 * @param timeUnit The time unit of the `timeout` parameter
 */
private fun doExecPipe(vararg cmdline: String,
                isSudo: Boolean = false,
                directory: File? = null,
                timeout: Long = 60L,
                timeUnit: TimeUnit = TimeUnit.SECONDS): Pair<ExecResult, String> {

    val command = when {
        isSudo -> arrayOf("sudo", *cmdline)
        else -> cmdline
    }

    log.debug { "Starting ${command.joinToString(" ")}"}

    val process = ProcessBuilder().let {
        it.command(*command)
        if (directory != null) it.directory(directory)
        it.redirectInput(ProcessBuilder.Redirect.PIPE)
        it.redirectError(ProcessBuilder.Redirect.PIPE)
        it.redirectOutput(ProcessBuilder.Redirect.PIPE)
        it.start()
    }

    try {
        if (timeout > 0) {
            if (!process.waitFor(timeout, timeUnit)) {
                return ExecTimeout(timeout, timeUnit, null) to ""
            }
        } else {
            process.waitFor()
        }
    } catch (interrupt: InterruptedException) {
        return ExecTimeout(timeout, timeUnit, interrupt) to ""
    }
    val exitCode = process.exitValue()
    val stderr = process.errorStream.bufferedReader().use { it.readText() }
    val stdout = process.inputStream.bufferedReader().use { it.readText() }

    log.debug { "cmd ${cmdline.joinToString(" ")} exit code $exitCode\nstdout:$stdout\nstderr: $stderr"}

    return if (exitCode == 0) {
        ExecOk to stdout
    } else {
        ExecFailed(exitCode) to stdout
    }
}

private val log = KotlinLogging.logger {}