package jekyllfy.exec

import mu.KotlinLogging
import java.io.File
import java.util.concurrent.TimeUnit


sealed class ExecResult
data class ExecTimeout(val timeout: Long, val timeUnit: TimeUnit, val interruptedException: InterruptedException?) : ExecResult()
data class ExecFailed(val exitCode: Int) : ExecResult()
object ExecOk : ExecResult()

interface ExecInherit {

    operator fun invoke(vararg cmdline: String,
                        directory: File? = null,
                        timeout: Long = 60L,
                        timeUnit: TimeUnit = TimeUnit.SECONDS): ExecResult
}

class ExecInheritInternal(val isSudo: Boolean): ExecInherit {

    override fun invoke(vararg cmdline: String, directory: File?, timeout: Long, timeUnit: TimeUnit) =
        doExecInherit(cmdline = *cmdline, isSudo = isSudo, directory = directory, timeout = timeout, timeUnit = timeUnit)
}

/**
 * Wraps the ProcessBuilder, and uses inherited IO
 *
 * @param cmdline The main command line
 * @param isSudo If true, just prepends "sudo" to the commandline
 * @param timeout If > 0, issues a timed wait before returning an ExecTImeout
 * @param timeUnit The time unit of the `timeout` parameter
 */
private fun doExecInherit(vararg cmdline: String,
                isSudo: Boolean = false,
                directory: File? = null,
                timeout: Long = 60L,
                timeUnit: TimeUnit = TimeUnit.SECONDS): ExecResult {

    val command = when {
        isSudo -> arrayOf("sudo", *cmdline)
        else -> cmdline
    }

    log.debug { "Starting ${command.joinToString(" ")}"}

    val process = ProcessBuilder().let {
        it.command(*command)
        if (directory != null) it.directory(directory)
        it.redirectInput(ProcessBuilder.Redirect.INHERIT)
        it.redirectError(ProcessBuilder.Redirect.INHERIT)
        it.redirectOutput(ProcessBuilder.Redirect.INHERIT)
        it.start()
    }

    try {
        if (timeout > 0) {
            if (!process.waitFor(timeout, timeUnit)) {
                return ExecTimeout(timeout, timeUnit, null)
            }
        } else {
            process.waitFor()
        }
    } catch (interrupt: InterruptedException) {
        return ExecTimeout(timeout, timeUnit, interrupt)
    }
    val exitCode = process.exitValue()

    return if (exitCode == 0) {
        ExecOk
    } else {
        ExecFailed(exitCode)
    }
}

private val log = KotlinLogging.logger { }