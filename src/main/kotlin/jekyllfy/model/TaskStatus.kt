package jekyllfy.model

/**
 * A lot of our "methods" can report failures that should basically halt processing.
 *
 * The type of this class is the "good" result type.
 */
sealed class TaskStatus<T> {

    fun orStop(): T = when (this) {
        is TaskOk -> result
        else -> throw StopException(this)
    }
}

/**
 * This is the "it's all good" result.
 */
data class TaskOk<T>(val result: T) : TaskStatus<T>()

/**
 * This captures cases where tasks halted due to a problem.
 *
 * Diagnostic messages should have already been sent.
 */
class TaskFailed<T>: TaskStatus<T>()


data class StopException(val taskStatus: TaskStatus<*>) : RuntimeException()