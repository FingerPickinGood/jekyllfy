package jekyllfy.model

import java.util.concurrent.TimeUnit

sealed class ExecResult
data class ExecTimeout(val timeout: Long, val timeUnit: TimeUnit, val interruptedException: InterruptedException?) : ExecResult()
data class ExecFailed(val exitCode: Int) : ExecResult()
object ExecOk : ExecResult()