package jekyllfy

import com.github.ajalt.clikt.core.CliktCommand
import jekyllfy.exec.*
import mu.KotlinLogging

class Clean(
        val execInherit: ExecInherit,
        val execPipe: ExecPipe
) : CliktCommand("Remove built image and containers") {

    override fun run() = runCommand("jekyllfy clean") {
        val images = listImages()
        images.forEach { image ->
            val containers = listContainers(image)
            containers.forEach { removeContainer(it) }
            removeImage(image)
        }
    }
}

private val log = KotlinLogging.logger { }

private fun Clean.listImages(): List<String> {
    val (result, stdout) = execPipe("docker", "images",
            "-f", "reference=jekyllfy:latest",
            "-q")
    return when (result) {
        is ExecOk -> stdout.lines().map { it.trim() }.filter { it.isNotEmpty() }
        is ExecFailed -> error("docker images failed with error code ${result.exitCode}: $stdout")
        is ExecTimeout -> error("docker images timed out")
    }
}

private fun Clean.listContainers(image: String): List<String> {
    val (result, stdout) = execPipe("docker", "ps",
            "-a",
            "-f", "ancestor=$image",
            "-q")
    return when (result) {
        is ExecOk -> stdout.lines().map { it.trim() }.filter { it.isNotEmpty() }
        is ExecFailed -> error("docker ps ... failed with error code ${result.exitCode}: $stdout")
        is ExecTimeout -> error("docker ps timed out")
    }
}

private fun Clean.removeImage(imageId: String) {
    val (result, stdout) = execPipe("docker", "rmi", imageId)
    when (result) {
        is ExecOk -> {
        }
        is ExecFailed -> error("docker rmi $imageId failed with ${result.exitCode}: $stdout")
        is ExecTimeout -> error("docker rmi timed out")
    }
}

private fun Clean.removeContainer(containerId: String) {
    val (result, stdout) = execPipe("docker", "rm", "-f", containerId)
    when (result) {
        is ExecOk -> {
        }
        is ExecFailed -> error("docker stop failed with exit ${result.exitCode}:\n$stdout")
        is ExecTimeout -> error("illegal timeout hit")
    }
}
