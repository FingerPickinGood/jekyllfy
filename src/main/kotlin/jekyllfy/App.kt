package jekyllfy

import ch.qos.logback.classic.Level
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.findObject
import com.github.ajalt.clikt.core.subcommands
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import jekyllfy.exec.ExecInheritInternal
import jekyllfy.exec.ExecPipeInternal
import mu.KotlinLogging
import org.slf4j.LoggerFactory
import java.io.File
import ch.qos.logback.classic.Logger as LogbackLogger
import org.slf4j.Logger as Slf4JLogger

private val log = KotlinLogging.logger { }

/**
 * This mostly just lets you tweak logging before doing the real work in any
 * subcommand.
 */
class App : CliktCommand(name = "jekyllfy", help ="Select a subcommand") {

    val info by option(help = "Increase logging messages to INFO").flag(default = false)
    val debug by option(help = "Increase logging messages to DEBUG").flag(default = false)
    val label by option(help = "The image label, default 'jekyllfy:latest'").default("jekyllfy:latest")
    val workdir by option(help = "The 'working' project directory, defaults to .").default(".")
    val config by findObject { mutableMapOf<String, String>() }

    val rootLogger:LogbackLogger by lazy {
        LoggerFactory.getLogger(Slf4JLogger.ROOT_LOGGER_NAME) as LogbackLogger
    }

    override fun run() {
        if (info) {
            rootLogger.level = Level.INFO
        }
        if (debug) {
            rootLogger.level = Level.DEBUG
        }

        // Pass these options down to the subcommands via the config map
        config["LABEL"] = label
        config["WORKING_DIR"] = File(workdir).canonicalPath
    }
}

fun main(args: Array<String>) {
    // Going to assume that your local user is just running something like
    // Docker for Mac or Docker for Windows.
    val useSudo = !isMac() && !isWindows()
    val execInherit = ExecInheritInternal(isSudo = useSudo)
    val execPipe = ExecPipeInternal(isSudo = useSudo)

    App().apply {
        subcommands(
                Clean(execInherit, execPipe),
                MkImage(execInherit, execPipe),
                Run(execInherit, execPipe))
        main(args)
    }
}

// Do some very minimal OS detection.
// This is _only_ focused on determining if we really want to use sudo.

private val osNameLC: String by lazy { System.getProperty("os.name").toLowerCase() }

private fun isMac(): Boolean =
    osNameLC.contains("mac") || osNameLC.contains("darwin")

private fun isWindows(): Boolean =
        osNameLC.contains("windows")
