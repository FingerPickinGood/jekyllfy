package jekyllfy

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.parameters.options.multiple
import com.github.ajalt.clikt.parameters.options.option
import jekyllfy.exec.*
import mu.KotlinLogging
import java.io.File

/**
 * The `mkimage` command effectively wraps the `docker build` command to create
 * our context image.
 *
 * This can take a TOML configuration file to indicate system packages to
 * include into the build image.
 */
class MkImage(
        val execInherit: ExecInherit,
        val execPipe: ExecPipe
) : CliktCommand("Creates Docker image for executing Jekyll") {

    val packages by option("-p", help = "Additional Ubuntu installation packages").multiple()
    val gems by option("-g", help = "Additional gem files to include").multiple()
    val userId by option("-uid", help = "Set user ID (default: this user)")
    val groupId by option("-gid", help = "Set group ID (default: this user's group)")
    val config by requireObject<Map<String, String>>()

    override fun run() = runCommand("jekyllfy mkimage") {
        log.debug { "packages: $packages" }
        log.debug { "gems: $gems" }
        val label = config["LABEL"] ?: error("LABEL is not set")
        log.debug { "image label: $label" }

        val dir = createTempDir("jekyllfy")
        try {
            val (uid, gid) = getUserInfo()
            createDockerfile(dir, uid, gid)
            executeDockerBuild(dir, label)
        } finally {
            dir.deleteRecursively()
        }
    }
}

private val log = KotlinLogging.logger { }

private fun MkImage.getUserInfo(): Pair<Int, Int> {
    val userName = System.getProperty("user.name") ?: throw IllegalStateException("System property `user.name` is null")
    val uid = userId?.toInt() ?: getId("-u", userName)
    val gid = groupId?.toInt() ?: getId("-g", userName)
    return uid to gid
}

private fun MkImage.getId(type: String, userName: String): Int {
    val (result, stdout) = execPipe("id", type, userName)
    return when (result) {
        is ExecOk -> stdout.trim().toInt()
        is ExecFailed -> throw IllegalStateException("`id -u $userName` failed " +
                "with code ${result.exitCode}\n" +
                "stdout:\n$stdout\n")
        is ExecTimeout -> throw IllegalStateException("`id -u $userName` timed out")
    }
}

/**
 * Generate our dockerfile with additional packages and gems as indicated by
 * the user.
 *
 * We try to set up the container to execute as a user account
 */
private fun MkImage.createDockerfile(dir: File, userId: Int, groupId: Int) {
    val file = dir.resolve("Dockerfile")
    file.writeText("""
            |FROM ubuntu:18.04
            |LABEL maintainer "jekyllfy <mr.tristan@gmail.com>"
            |RUN if getent passwd jekyllfy ; then userdel -f jekyllfy; fi &&\
            |    if getent group jekyllfy ; then groupdel jekyllfy; fi &&\
            |    groupadd -g $groupId jekyllfy &&\
            |    useradd -l -u $userId -g jekyllfy jekyllfy &&\
            |    install -d -m 0755 -o jekyllfy -g jekyllfy /home/jekyllfy
            |RUN apt-get update -y
            |RUN apt-get install -y ruby-full build-essential zlib1g-dev
            ${packages.joinToString("\n") { "|RUN apt-get install -y $it" }}
            |USER $userId:$groupId
            |ENV GEM_HOME="/home/jekyllfy/gems"
            |ENV PATH="/home/jekyllfy/gems/bin:${"$"}PATH"
            |RUN gem install jekyll bundler jekyll-asciidoc
            ${gems.joinToString("\n") { "|RUN gem install $it" }}
    """.trimMargin())
    log.debug { "Dockerfile:\n${file.readText()}" }
}

private fun MkImage.executeDockerBuild(dir: File, label: String) {
    val cmdline = arrayOf("docker", "build",
            "--force-rm",       // remove intermediate containers
            "--tag", label,     // set our metadata
            ".")                // run in dir

    when (val result = execInherit(*cmdline, timeout = 0, directory = dir)) {
        is ExecOk -> log.debug { "${cmdline.joinToString(" ")} passed" }
        is ExecFailed -> throw IllegalStateException(
                "command failed with exit code ${result.exitCode}:\n" +
                        "command: ${cmdline.joinToString(" ")}\n")
        is ExecTimeout -> throw IllegalStateException("command timed out (when timeout was disabled)")
    }
}