package jekyllfy

import mu.KotlinLogging
import kotlin.system.exitProcess

fun runCommand(name: String, run: () -> Unit) {
    try {
        run()
    } catch (th: Throwable) {
        if (log.isDebugEnabled) {
            log.debug(th) { "$name failed" }
        } else {
            System.err.println("$name failed with error: ${th.message}\n" +
                    "Use --debug option to see stack trace")
        }
        exitProcess(1)
    }
}

private val log = KotlinLogging.logger { }